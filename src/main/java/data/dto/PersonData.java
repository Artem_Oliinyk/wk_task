package data.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class PersonData {

    private static final String EMAIL_PATTERN = "([A-Za-z0-9]|(\\.{1})[A-Za-z0-9])+\\@wolterskluwer\\.com$";
    private static final String PHONE_PATTERN = "^[\\d\\s-]{10,11}$";
    private static final String UNID_PATTERN = "^[0-9]{7}$";

    @Pattern(message = "UNID has invalid format: ${validatedValue}", regexp = UNID_PATTERN)
    @NotNull(message = "unid is null")
    private String unid;

    @NotNull(message = "firstName is null")
    private String firstName;

    @NotNull(message = "lastName is null")
    private String lastName;

    @Email(message = "Email address has invalid format: ${validatedValue}",
            regexp = EMAIL_PATTERN,
            flags = Pattern.Flag.CASE_INSENSITIVE)
    @NotNull(message = "emailAddress is null")
    private String emailAddress;

    @Pattern(message = "Phone number has invalid format: ${validatedValue}", regexp = PHONE_PATTERN)
    @NotNull(message = "phoneNumber is null")
    private String phoneNumber;

    private String countryCode;

    private boolean consentForDetails;

    private boolean marketingConsent;

}
