package data.dto;

import lombok.Getter;

@Getter
public class Source {
    String source;
    PersonData data;
}
