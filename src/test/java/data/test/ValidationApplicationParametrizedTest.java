package data.test;

import data.dto.PersonData;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

@RunWith(value = Parameterized.class)
public class ValidationApplicationParametrizedTest {

    private static Validator validator;

    @Parameterized.Parameter(0)
    public String unid;
    @Parameterized.Parameter(1)
    public String firstName;
    @Parameterized.Parameter(2)
    public String lastName;
    @Parameterized.Parameter(3)
    public String emailAddress;
    @Parameterized.Parameter(4)
    public String phoneNumber;

    @Parameterized.Parameters(name = "{index}: testAdd({0}+{1}) = {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null,"John","Doe","John.Doe@wolterskluwer.com","1-12-45-789"},
                {"1234567",null,"Doe","John.Doe@wolterskluwer.com","1-12-45-789"},
                {"1234567","John",null,"John.Doe@wolterskluwer.com","1-12-45-789"},
                {"1234567","John","Doe",null,"1-12-45-789"},
                {"1234567","John","Doe","John.Doe@wolterskluwer.com",null},
        });
    }

    @BeforeClass
    public static void init() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    private PersonData createPersonData(){
        PersonData personData = new PersonData();
        personData.setUnid(unid);
        personData.setFirstName(firstName);
        personData.setLastName(lastName);
        personData.setEmailAddress(emailAddress);
        personData.setPhoneNumber(phoneNumber);
        return personData;
    }

    @Test
    public void checkNotNullFieldsTest() {
        Set<ConstraintViolation<PersonData>> validates = validator.validate(createPersonData());
        ConstraintViolation constraintViolation =  validates.iterator().next();

        Assert.assertFalse(validates.isEmpty());
        Assert.assertEquals(
                constraintViolation.getMessage(),
                String.format("%s is null", constraintViolation.getPropertyPath().toString()));
    }
}
