package data.test;

import com.google.gson.Gson;
import data.dto.PersonData;
import data.dto.Source;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Set;

public class ValidationApplicationTest {
    private static final String FILE_PATH = "src/main/resources/data.json";

    private static Validator validator;
    private Source source;

    @BeforeClass
    public static void init() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Before
    public void initSource() throws FileNotFoundException {
        FileReader fileReader = new FileReader(FILE_PATH);
        source = new Gson().fromJson(fileReader, Source.class);
    }

    @Test
    public void checkValidityOfUnidValue() {
        source.getData().setUnid("123456789");
        Set<ConstraintViolation<PersonData>> validates = validator.validate(source.getData());

        Assert.assertFalse(validates.isEmpty());
        Assert.assertEquals(validates.iterator().next().getMessage(), "UNID has invalid format: 123456789");
    }

    @Test
    public void checkValidityOfEmailValue() {
        source.getData().setEmailAddress("John_Doe@wolterskluwer.com");
        Set<ConstraintViolation<PersonData>> validates = validator.validate(source.getData());

        Assert.assertFalse(validates.isEmpty());
        Assert
                .assertEquals(
                        validates.iterator().next().getMessage(),
                        "Email address has invalid format: John_Doe@wolterskluwer.com");
    }

    @Test
    public void checkValidityOfPhoneValue() {
        source.getData().setPhoneNumber("1-22-55-3388");
        Set<ConstraintViolation<PersonData>> validates = validator.validate(source.getData());

        Assert.assertFalse(validates.isEmpty());
        Assert.assertEquals(
                validates.iterator().next().getMessage(),
                "Phone number has invalid format: 1-22-55-3388");
    }

    @Test
    public void checkValidityOfPersonData() {
        Set<ConstraintViolation<PersonData>> validates = validator.validate(source.getData());

        Assert.assertTrue(validates.isEmpty());
    }


}

